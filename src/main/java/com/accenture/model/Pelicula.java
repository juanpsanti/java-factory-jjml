package com.accenture.model;

public class Pelicula {
	private int id;
	private String titulo;
	private String idioma_original;
	private String descripcion;
	private String ano;
	private float puntaje;
	private float recomendado;
	public Pelicula() {
	}
	public Pelicula(int id, String titulo, String idioma_original, String descripcion, String ano, float puntaje) {
		this.id = id;
		this.titulo = titulo;
		this.idioma_original = idioma_original;
		this.descripcion = descripcion;
		this.ano = ano;
		this.puntaje = puntaje;
		this.recomendado = 1;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getIdioma_original() {
		return idioma_original;
	}
	public void setIdioma_original(String idioma_original) {
		this.idioma_original = idioma_original;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public float getPuntaje() {
		return puntaje;
	}
	public void setPuntaje(float puntaje) {
		this.puntaje = puntaje;
	}
	public float getRecomendado() {
		return recomendado;
	}
	public void setRecomendado(int recomendado) {
		this.recomendado = recomendado;
	}
	public float addRecomendado() {
		return recomendado += 0.25;
	}
	
}
