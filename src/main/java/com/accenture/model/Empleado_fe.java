package com.accenture.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Proxy;
import org.hibernate.validator.constraints.Length;

@Proxy(lazy=false)
@Entity
public class Empleado_fe {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long idEmpleado;
	@Column(length = 80, nullable = false)
	@Length(min = 1, max = 80, message = "The field must be less than 80 characters")
	@NotNull
	private String nombre;
	@Column(length = 80, nullable = false)
	@Length(min = 1,max = 80, message = "The field must be less than 80 characters")
	@NotNull
	private String apellido;
	@Column(length = 150, nullable = false)
	@Length(min = 1,max = 150, message = "The field must be less than 150 characters")
	@NotNull
	private String idAccenture;
	@Column(length = 150, nullable = false)
	@Length(min = 1,max = 150, message = "The field must be less than 150 characters")
	@NotNull
	private String mail;
	@Column(length = 6)
	@Length(max = 6, message = "The field must be less than 6 characters")
	private String seat;
	@Column(length = 100)
	@Length(max = 100, message = "The field must be less than 100 characters")
	private String dias_home;
	private byte estado;
	/*
		EMPLEADO_FE que contenga los campos: IDEMPLEADO (LONG, NOMBRE (VARCHAR 80), APELLIDO (VARCHAR 80), 
	IDACCENTURE (VARCHAR 150), MAIL(VARCHAR 150), SEAT(VARCHAR 6), DIAS_HOME (VARCHAR 100), ESTADO(BYTE, 0 'INACTIVO', 1 'ACTIVO')
	Aclaraciones: DIAS_HOME es una lista de nombres separado por coma.
*/
	public Empleado_fe() {
		
	}

	public long getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getIdAccenture() {
		return idAccenture;
	}

	public void setIdAccenture(String idAccenture) {
		this.idAccenture = idAccenture;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getDias_home() {
		return dias_home;
	}

	public void setDias_home(String dias_home) {
		this.dias_home = dias_home;
	}

	public byte getEstado() {
		return estado;
	}

	public void setEstado(byte estado) {
		this.estado = estado;
	}
	
	
}
