package com.accenture.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Proxy;
import org.hibernate.validator.constraints.Length;


/*Crear una  nueva TABLA en la base ENLACES que contenga los campos: 
 IDENLACE
 NOMBRE,
 CODIGO, 
 POS_LONG, 
 POS_LAT, 
 GRA_LONG, 
 GRAD_LAT, 
 MIN_LONG, 
 MIN_LAT, 
 SEG_LONG, 
 SEG_LAT, 
 ESTADO.
 */

@Proxy(lazy=false)
@Entity
public class Enlaces {
	@NotNull
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	
	private long id;
	
	@Column(length = 80, nullable = false)
	@Length(min = 1, max = 80)
	@NotNull
	private String nombre;
	
	@NotNull
	
	private String codigo;
	@NotNull
	
	private int pos_log;
	@NotNull


	private int pos_lat;
	@NotNull

	private int gra_lat;

	@NotNull
	private int gra_long;

	private int min_lat;
	
	@NotNull
	private int min_long;

	@NotNull
	private int sec_lat;

	@NotNull
	private int sec_long;
	
	private byte estado;
	
	
	public long getIdEnlace() {
		return id;
	}
	public void setIdEnlace(long idEnlace) {
		this.id = idEnlace;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public int getPos_log() {
		return pos_log;
	}
	public void setPos_log(int pos_log) {
		this.pos_log = pos_log;
	}
	public int getPos_lat() {
		return pos_lat;
	}
	public void setPos_lat(int pos_lat) {
		this.pos_lat = pos_lat;
	}
	public int getGra_lat() {
		return gra_lat;
	}
	public void setGra_lat(int gra_lat) {
		this.gra_lat = gra_lat;
	}
	public int getGra_long() {
		return gra_long;
	}
	public void setGra_long(int gra_long) {
		this.gra_long = gra_long;
	}
	public int getMin_lat() {
		return min_lat;
	}
	public void setMin_lat(int min_lat) {
		this.min_lat = min_lat;
	}
	public int getMin_long() {
		return min_long;
	}
	public void setMin_long(int min_long) {
		this.min_long = min_long;
	}
	public int getSec_lat() {
		return sec_lat;
	}
	public void setSec_lat(int sec_lat) {
		this.sec_lat = sec_lat;
	}
	public int getSec_long() {
		return sec_long;
	}
	public void setSec_long(int sec_long) {
		this.sec_long = sec_long;
	}
	public byte getEstado() {
		return estado;
	}
	public void setEstado(byte estado) {
		this.estado = estado;
	}
	
	
	
}
