package com.accenture.service;

public interface IWSCalculosService {

	boolean isVerificacion();

	String getNumeroVerificador();

	void setNumeroVerificador(String numeroVerificador);

	int getNumeroResultado();

	void setNumeroResultado(int numeroResultado);

	void setValidacion(boolean verificacion);

	String getNumeroControlar();

	void setNumeroControlar(String numeroControlar);

	void validar();

	boolean isValidar();

	void setValidar(boolean validar);

	void calcular();

}