package com.accenture.service;

import java.util.List;
import com.accenture.model.Empleado_fe;

public interface IWSConsulta {

	public Empleado_fe getEmpleado(long id);
	public String getNombreEmp(long id);
	public String getApellidoEmp(long id);
	public String getMailEmp(long id);
	public String getSeatEmp(long id);
	public String getDiasEmp(long id);
	public byte getEstadoEmp(long id);
	public List<Empleado_fe> listaEmpleado();
	
}
