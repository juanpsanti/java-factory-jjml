package com.accenture.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.data.Empleado_feDAO;
import com.accenture.model.Empleado_fe;

@Service
public class WSPersistencia {
	@Autowired
	Empleado_feDAO empleadoDAO;

	public Empleado_fe bajaEmpleado(Long id){
		Empleado_fe empleado;
		try {
			empleado = empleadoDAO.getEmpleado_fe(id);
		} catch (Exception e) {
			return null;
		}
		empleado.setEstado((byte) 0);
		empleadoDAO.updateEmpleado_fe(empleado);
		return empleado;
	}
	
	public Empleado_fe reactivarEmpleado(Long id){
		Empleado_fe empleado;
		try {
			empleado = empleadoDAO.getEmpleado_fe(id);
		} catch (Exception e) {
			return null;
		}
		empleado.setEstado((byte) 1);
		empleadoDAO.updateEmpleado_fe(empleado);
		return empleado;
	}
	
	public void altaEmpleado(Empleado_fe empleado){
		empleado.setEstado((byte) 1);
		empleadoDAO.insertEmpleado_fe(empleado);
	}
	
	public Empleado_fe modificarEmpleado(Long id, String nombre, String apellido, String seat, String dias_home){
		Empleado_fe empleado;
		try {
			empleado = empleadoDAO.getEmpleado_fe(id);
		} catch (Exception e) {
			return null;
		}
		if (nombre != null) {
			empleado.setNombre(nombre);
		}
		if (apellido != null) {
			empleado.setApellido(apellido);
		}
		if (seat != null) {
			empleado.setSeat(seat);
		}
		if (dias_home != null) {
			empleado.setDias_home(dias_home);
		}
		empleadoDAO.updateEmpleado_fe(empleado);
		return empleado;
	}
}
