package com.accenture.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accenture.data.Empleado_feDAO;
import com.accenture.model.Empleado_fe;

public class WSConsulta implements IWSConsulta{

	@Autowired
	Empleado_feDAO empleado_feDAO;
	
	public Empleado_fe getEmpleado(long id) {
	
		return empleado_feDAO.getEmpleado_fe(id);
	}

	public String getNombreEmp(long id) {
		return empleado_feDAO.getEmpleado_fe(id).getNombre();
	}

	public String getApellidoEmp(long id) {
		return empleado_feDAO.getEmpleado_fe(id).getApellido();
	}

	public String getMailEmp(long id) {
		return empleado_feDAO.getEmpleado_fe(id).getMail();
	}

	public String getSeatEmp(long id) {
		return empleado_feDAO.getEmpleado_fe(id).getSeat();
	}

	public String getDiasEmp(long id) {
		return empleado_feDAO.getEmpleado_fe(id).getDias_home();
	}

	public byte getEstadoEmp(long id) {
		return empleado_feDAO.getEmpleado_fe(id).getEstado();
	}

	public List<Empleado_fe> listaEmpleado() {
		return empleado_feDAO.listEmpleado_fe();
	}

	
}
