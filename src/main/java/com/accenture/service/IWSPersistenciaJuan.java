package com.accenture.service;

import com.accenture.model.Enlaces;

public interface IWSPersistenciaJuan {

	Enlaces getEnlaces(Long id);

	void updateEnlaces(Enlaces enlace);

	void deleteEnlaces(Long id);

	void insertEnlaces(Enlaces enlace);

}