package com.accenture.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.data.TestDAO;
import com.accenture.model.Test;

@Service
public class TestService implements ITestService {
	@Autowired
	TestDAO testDAO;
	
	public Test getTest(Long id){
		return testDAO.getTest(id);
	}
	
	public String getTestName(Long id){
		return testDAO.getTest(id).getNombre();
	}
	
	public List<Test> listTest(){
		return testDAO.listTest();
	}
	

	public void updateTest(Test test){
		testDAO.updateTest(test);
	}
	

	public void deleteTest(Long id){
		testDAO.deleteTest(id);
	}
	

	public void insertTest(Test test){
		testDAO.insertTest(test);
	}
}
