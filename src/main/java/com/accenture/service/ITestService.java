package com.accenture.service;

import java.util.List;

import com.accenture.model.Test;

public interface ITestService {
	public Test getTest(Long id);
	public String getTestName(Long id);
	public List<Test> listTest();
	public void updateTest(Test test);
	public void deleteTest(Long id);
	public void insertTest(Test test);
}
