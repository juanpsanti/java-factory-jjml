package com.accenture.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.data.EnlacesDAO;
import com.accenture.model.Enlaces;

@Service
public class WSpersistenciaJuan implements IWSPersistenciaJuan{
	
	@Autowired
	EnlacesDAO enlaceDAO;

	/* (non-Javadoc)
	 * @see com.accenture.service.IWSPersistenciaJuan#getEnlaces(java.lang.Long)
	 */
	/* (non-Javadoc)
	 * @see com.accenture.service.hujkchkj#getEnlaces(java.lang.Long)
	 */
	public Enlaces getEnlaces(Long id){
		return enlaceDAO.getEnlaces(id);
	}
	
	
	/* (non-Javadoc)
	 * @see com.accenture.service.IWSPersistenciaJuan#updateEnlaces(com.accenture.model.Enlaces)
	 */
	/* (non-Javadoc)
	 * @see com.accenture.service.hujkchkj#updateEnlaces(com.accenture.model.Enlaces)
	 */
	public void updateEnlaces(Enlaces enlace){
		enlaceDAO.updateEnlaces(enlace);
	}

	/* (non-Javadoc)
	 * @see com.accenture.service.IWSPersistenciaJuan#deleteEnlaces(java.lang.Long)
	 */
	/* (non-Javadoc)
	 * @see com.accenture.service.hujkchkj#deleteEnlaces(java.lang.Long)
	 */
	public void deleteEnlaces(Long id){
		enlaceDAO.deleteEnlaces(id);
	}
	
	/* (non-Javadoc)
	 * @see com.accenture.service.IWSPersistenciaJuan#insertEnlaces(com.accenture.model.Enlaces)
	 */
	/* (non-Javadoc)
	 * @see com.accenture.service.hujkchkj#insertEnlaces(com.accenture.model.Enlaces)
	 */
	public void insertEnlaces(Enlaces enlace){
		enlaceDAO.insertEnlaces(enlace);
	}

}
