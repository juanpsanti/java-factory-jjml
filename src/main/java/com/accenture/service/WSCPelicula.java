package com.accenture.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.springframework.stereotype.Service;

import com.accenture.model.Pelicula;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;



@Service
public class WSCPelicula {
	ObjectMapper mapper = new ObjectMapper();
	JsonNodeFactory nodeFactory = new JsonNodeFactory(false);
	public  String GetPelicula(String nombre){
		try {
			URL url = new URL("http://api.themoviedb.org/3/search/movie?query="+nombre.toLowerCase()+"&api_key=e6fa44d7532ce636886f5164b45e4794");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			InputStream is = con.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line = rd.readLine();
			rd.close();
			ObjectNode node = mapper.readValue(line, ObjectNode.class);
			JsonNode resultNode = node.path("results").path(0);
			JsonNodeFactory nodeFactory = new JsonNodeFactory(false);
			ObjectNode nodeOut = nodeFactory.objectNode();
			nodeOut.put("titulo", resultNode.get("original_title"));
			nodeOut.put("idioma_original", resultNode.get("original_language"));
			nodeOut.put("descripcion", resultNode.get("overview"));
			nodeOut.put("anio", resultNode.get("release_date").asText().substring(0,4));
			nodeOut.put("puntaje", resultNode.get("vote_average"));
			return nodeOut.toString();
		} catch (Exception e) {
			
		}
		return "{}";
	}
	public  String GetSimilar(String listaId){
		try {
			ObjectNode nodeId = mapper.readValue(listaId, ObjectNode.class);
			JsonNode resultNodeIds = nodeId.path("ultVistas");
			ObjectNode nodeList = nodeFactory.objectNode();
			ArrayNode elArray = nodeList.putArray("sugerencias");
			ArrayList<String> idList = new ArrayList<String>();
			ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
			
			for (int i = 0; i < resultNodeIds.size(); i++) {
				String movieId = resultNodeIds.get(i).asText();
				idList.add(movieId);
			}
			for (String movieId : idList) {
				try {
					URL url = new URL("http://api.themoviedb.org/3/movie/"+movieId+"/similar?api_key=b4869b04991825f921191fcd16343d03");
					HttpURLConnection con = (HttpURLConnection) url.openConnection();
					InputStream is = con.getInputStream();
					BufferedReader rd = new BufferedReader(new InputStreamReader(is));
					String line = rd.readLine();
					rd.close();
					System.out.println(line);
					ObjectNode node = mapper.readValue(line, ObjectNode.class);
					JsonNode resultNode = node.path("results");
					for (int i = 0; i < resultNode.size(); i++) {
						JsonNode arrayNode = resultNode.path(i);
						boolean inList = false;
						for (String idMovie : idList) {
							if (idMovie.equals(arrayNode.get("id").asText())) {
								inList = true;
							}
						}
						if (!inList) {
							Pelicula laPelicula = null;
							for (Pelicula laSimilar : peliculas) {
								if (laSimilar.getId() == arrayNode.get("id").asInt()) {
									laPelicula = laSimilar;
								}
							}
							if (laPelicula != null) {
								laPelicula.addRecomendado();
							} else {
								laPelicula = new Pelicula(arrayNode.get("id").asInt(), arrayNode.get("original_title").asText(), arrayNode.get("original_language").asText(), arrayNode.get("overview").asText(), arrayNode.get("release_date").asText(), (float)arrayNode.get("vote_average").asDouble());
								peliculas.add(laPelicula);
							}
						}
					}
				} catch (Exception e) {
				}
			}
			Comparator<Pelicula> comparator = new Comparator<Pelicula>() {
				 public int compare(Pelicula p1, Pelicula p2) {
		            return (p1.getPuntaje()*p1.getRecomendado() < p2.getPuntaje()*p2.getRecomendado() ? 1 : -1);
		        }
			};
			Comparator<Pelicula> comparator2 = new Comparator<Pelicula>() {
				 public int compare(Pelicula p1, Pelicula p2) {
		            return (p1.getPuntaje() < p2.getPuntaje() ? 1 : -1);
		        }
			};
			Collections.sort(peliculas, comparator);
			ArrayList<Pelicula> mejores = new ArrayList<Pelicula>();
			for (Pelicula pelicula : peliculas) {
				mejores.add(pelicula);
				if (peliculas.indexOf(pelicula) >= 19) {
					break;
				}
			}
			Collections.sort(mejores, comparator2);
			for (Pelicula pelicula : mejores) {
				System.out.println(pelicula.getTitulo() + " - " + pelicula.getPuntaje() + " - " + pelicula.getRecomendado());
				ObjectNode nodeOut = nodeFactory.objectNode();
				nodeOut.put("titulo", pelicula.getTitulo());
				nodeOut.put("idioma", pelicula.getIdioma_original());
				nodeOut.put("descripcion", pelicula.getDescripcion());
				nodeOut.put("anio", pelicula.getAno().substring(0,4));
				nodeOut.put("puntaje", pelicula.getPuntaje());
				elArray.add(nodeOut);
			}
			return nodeList.toString();
		} catch (Exception e) {
		}
		return "{}";
	}
}
