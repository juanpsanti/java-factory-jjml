package com.accenture.service;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WSCuitService {

	@JsonIgnore
	private String entidad;
	private String ent;	
	private String cuit;
	@JsonIgnore
	private String verificador;
	private int digitoVerificador;
	private boolean verifica = false;
	@JsonIgnore
	private int[] lista = new int[10];




	public boolean isVerifica() {
		return verifica;
	}
	public void setVerifica(boolean verifica) {
		this.verifica = verifica;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getVerificador() {
		return verificador;
	}
	public void setVerificador(String verificador) {
		this.verificador = verificador;
	}
	public int getDigitoVerificador() {
		return digitoVerificador;
	}
	public void setDigitoVerificador(int digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}

	public int[] getLista() {
		return lista;
	}
	public void setLista(int[] lista) {
		this.lista = lista;
	}
	public int[] LlenarLista(){

		if (entidad.length() == 2) {
			lista[0] = (int) (entidad.charAt(0) - 48);
			lista[0]= lista[0] * 5;
			lista[1] = (int) (entidad.charAt(1) - 48);
			lista[1]= lista[1] * 4;
			System.out.println("llenar entidad");
		}
		if (cuit.length() == 7) {
			lista[2] = 0; 
			lista[3]=(cuit.charAt(0) - 48); 
			lista[4]=(cuit.charAt(1) - 48); 
			lista[5]=(cuit.charAt(2) - 48); 
			lista[6]=(cuit.charAt(3) - 48); 
			lista[7]=(cuit.charAt(4) - 48); 
			lista[8]=(cuit.charAt(5) - 48); 
			lista[9]=(cuit.charAt(6) - 48);
			System.out.println("llenar cuit 7");
		}
		else {

			lista[2]= (int) (cuit.charAt(0) - 48); 
			lista[2]= lista[2] * 3;
			lista[3]= (int) (cuit.charAt(1) - 48);
			lista[3]= lista[3] * 2;
			lista[4]= (int) (cuit.charAt(2) - 48);
			lista[4]= lista[4] * 7;
			lista[5]= (int) (cuit.charAt(3) - 48);
			lista[5]= lista[5] * 6;
			lista[6]= (int) (cuit.charAt(4) - 48);
			lista[6]= lista[6] * 5;
			lista[7]= (int) (cuit.charAt(5) - 48);
			lista[7]= lista[7] * 4;
			lista[8]= (int) (cuit.charAt(6) - 48);
			lista[8]= lista[8] * 3;
			lista[9]= (int) (cuit.charAt(7) - 48);
			lista[9]= lista[9] * 2;
			System.out.println("llenar cuit 8");

		}	
		return lista;
	}


	public int Validar(){
		LlenarLista();
		int suma = 0;
		if (entidad.equals("20")) {
			ent = "Hombre";
		}
		else
		{
			if (entidad.equals("27")) {
				ent = "Mujer";
			}
			else{
				ent = "Empresas";
			}
		}
		for (int j = 0; j < lista.length; j++) {
			suma = suma + lista[j];
		}
		System.out.println(suma);
		digitoVerificador = suma %11;
		if (digitoVerificador == 0) {
			digitoVerificador = 0;
		}	
		else
		{
			if (digitoVerificador == 1) {
				digitoVerificador = 9;
			}

			else
			{
				digitoVerificador = 11 - digitoVerificador;
			}
		}
		if (Integer.toString(digitoVerificador).equals(verificador)) {
			verifica = true;
		}
		return digitoVerificador;
	}

	public String getEnt() {
		return ent;
	}
	public void setEnt(String ent) {
		this.ent = ent;
	}	

}



