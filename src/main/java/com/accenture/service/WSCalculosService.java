package com.accenture.service;
import org.springframework.stereotype.Service;

@Service
public class WSCalculosService implements IWSCalculosService{


	private String numeroControlar;
	private String numeroVerificador;
	private int numeroResultado;
	private boolean verificacion;
	private boolean validar;


	public boolean isVerificacion() {
		return verificacion;
	}
	public String getNumeroVerificador() {
		return numeroVerificador;
	}
	public void setNumeroVerificador(String numeroVerificador) {
		this.numeroVerificador = numeroVerificador;
	}
	public int getNumeroResultado() {
		return numeroResultado;
	}
	public void setNumeroResultado(int numeroResultado) {
		this.numeroResultado = numeroResultado;
	}
	public void setValidacion(boolean verificacion) {
		this.verificacion = verificacion;
	}
	public String getNumeroControlar() {
		return numeroControlar;
	}
	public void setNumeroControlar(String numeroControlar) {
		this.numeroControlar = numeroControlar;
	}
	public void validar(){
		if (numeroControlar.length() == 11 && numeroControlar.matches("[0-9]*") && numeroVerificador.matches("[0-9]*" )){
			validar = true;
		}
	}
	public boolean isValidar() {
		return validar;
	}
	public void setValidar(boolean validar) {
		this.validar = validar;
	}


	//metodo que realiza los calculos, dado un numero de 11 digitos y un numero verificador, devuelve el numero verificador correcto
	// y si el numero verificador coincide con el dado. Ademas valida lo ingresado.
	public void calcular(){
		int resultado = 0;
		String auxiliar = null;
		int digitos = numeroControlar.toString().length();
		int[] listita = new int[11];
		if (digitos == 11) {
			auxiliar = numeroControlar.toString();
			for (int i = 0; i < 11; i++) {
				int n = (int) (auxiliar.charAt(i) - 48);
				listita[i] = n;
			}
			System.out.println("NUMERO CORRECTO");
		}
		else{
			System.out.println("SE NECESITAN 11 DIGITOS");
		}
		int sum1 = listita[0]+listita[2]+listita[4]+listita[6]+listita[8]+listita[10];
		sum1 = sum1 * 3;
		sum1 = sum1+listita[1]+listita[3]+listita[5]+listita[7]+listita[9];


		if ((sum1 + 0)%10 == 0){
			resultado=0;
		}
		if ((sum1 + 1)%10 == 0){
			resultado=1;
		}
		if ((sum1 + 2)%10 == 0){
			resultado=2;
		}
		if ((sum1 + 3)%10 == 0){
			resultado=3;
		}
		if ((sum1 + 4)%10 == 0){
			resultado=4;
		}
		if ((sum1 + 5)%10 == 0){
			resultado=5;
		}	
		if ((sum1 + 6)%10 == 0){
			resultado=6;
		}	
		if ((sum1 + 7)%10 == 0){
			resultado=7;
		}
		if ((sum1 + 8)%10 == 0){
			resultado=8;
		}	
		if ((sum1 + 9)%10 == 0){
			resultado=9;
		}
		numeroResultado = resultado;
		int	numeroVerificado =  Integer.parseInt(numeroVerificador);
		if (numeroResultado == numeroVerificado) {
			verificacion =true;
		}
	}
}

