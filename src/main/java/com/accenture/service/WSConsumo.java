package com.accenture.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WSConsumo implements IWSConsumo {

	public  String GetCountryByCountryCode(String parametros){
		try {
			URL url = new URL("http://www.webservicex.net/country.asmx/GetCountryByCountryCode?"+parametros.toLowerCase());
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// set propiedades del get
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setRequestProperty("Accept", "application/json;charset=UTF-8");



			//Get Response	
			InputStream is = con.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer(); 
			while((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			String re = response.toString();
			re = re.replace("&lt;", "<");
			re = re.replace("&gt;", ">");
			re = (re.split("<name>",2))[1];
			re = (re.split("</name>",2))[0];

			return re;

		} catch (Exception e) {		      
			//return (e.toString());
			return "El pais no existe";
		}
	}
}
