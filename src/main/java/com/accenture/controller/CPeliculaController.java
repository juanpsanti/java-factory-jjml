package com.accenture.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.service.WSCPelicula;

@Controller
@RequestMapping("pelicula")
public class CPeliculaController {
	@Autowired
	WSCPelicula wsCPelicula;

	@RequestMapping(value = "/{nombre}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String Detalles(@PathVariable("nombre")String nombre){
		String s = wsCPelicula.GetPelicula(nombre);
		return s;
	}
	
	@RequestMapping(value = "/recomendacion", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> Recomendacion(@RequestBody String listaId, BindingResult errors) {
		if (errors.hasErrors()) {
			return new ResponseEntity<String>(listaId, HttpStatus.BAD_REQUEST);
		}
		String similar = wsCPelicula.GetSimilar(listaId);
		return new ResponseEntity<String>(similar, HttpStatus.OK);
	}

}