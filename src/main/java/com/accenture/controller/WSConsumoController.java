package com.accenture.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.service.IWSConsumo;

@Controller
@RequestMapping("wsconsumo")
public class WSConsumoController {
	
	@Autowired
	IWSConsumo wsConsumo;

	@RequestMapping(value = "/GetNombrePaisPorCodigo/{nombre}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String Slash(@PathVariable("nombre")String nombre){
		
		String s = wsConsumo.GetCountryByCountryCode("CountryCode="+nombre);
		String ret = "{\"NombrePais\":\""+s+"\"}";
		return ret;
	}

}
