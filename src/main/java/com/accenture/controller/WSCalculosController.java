package com.accenture.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.model.Empleado_fe;
import com.accenture.model.Test;
import com.accenture.service.IWSCalculosService;
import com.accenture.service.WSCalculosService;
import com.accenture.service.WSCuitService;

import netscape.javascript.JSObject;

@Controller
public class WSCalculosController {
	
	@Autowired
	IWSCalculosService wsCalculosService;

	@RequestMapping(value = "Calcular", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String Slash(){
		return "Welcome"
				+ "/Controlar= 'INGRESAR NUMERO' &Verificar= 'INGRESAR NUMERO' ";
	}

	@RequestMapping(value = "/Controlar={numero}&Verificar={verificador}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody IWSCalculosService Calculo(@PathVariable("numero")String numero,@PathVariable("verificador")String verificador){
		IWSCalculosService c = new WSCalculosService();
		try {
			c.setNumeroControlar(numero);
			c.setNumeroVerificador(verificador);
			c.validar();
			if (c.isValidar()) {
			c.calcular();
			c.getNumeroVerificador();
			c.isVerificacion();
			}
		} catch (Exception e) { 
		}
		
				return c;
	}
	
	
	@RequestMapping(value = "/Cuit={numero}&Entidad={entidad}&Verificador={verificador}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody WSCuitService Calculo(@PathVariable("numero")String numero,@PathVariable("entidad")String entidad,@PathVariable("verificador")String verificador){
		WSCuitService c = new WSCuitService();
		try {
			c.setCuit(numero);
			c.setEntidad(entidad);
			c.setVerificador(verificador);
			
			c.Validar();
			}
		 catch (Exception e) { 
		}
		
				return c;
	}
	/*

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Test> Index2(){
		List<Test> t = new ArrayList<Test>();
		try {
			t = testService.listTest();
		} catch (Exception e) {

		}
		return t;
	}

	@RequestMapping(value = "/del={id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String Index3(@PathVariable("id")Long id){

		try {
			testService.deleteTest(id);
			return "Uds borro el test "+id+".";
		} catch (Exception e){
			return "Se produjo un error";
		}

	}


	@RequestMapping(value = "/h", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String help(){


		return "/list\t\t lista todos \n"
				+ "/del={id}\t borra \n"
				+ "/id={id}\t muestra solo uno \n";
	}
	@RequestMapping(value = "/name={name}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String Index(@PathVariable("name")String name){
		
		
		return "Hola, como estas"+name+"?";
	}
*/

}

