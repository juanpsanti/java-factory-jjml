package com.accenture.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.model.Enlaces;
import com.accenture.service.IWSPersistenciaJuan;

@Controller
@RequestMapping("persistenciaEnlaces")
public class WSPersistenciaJuanController {
	

	@Autowired
	IWSPersistenciaJuan wsPersistenciaJuan;
	
	

	@RequestMapping(value="/baja/{id}" , method=RequestMethod.GET)
	public @ResponseBody String Baja(@PathVariable("id")String id){
		String mensaje;
		try {
			Enlaces enlaces = new Enlaces();
			enlaces = (wsPersistenciaJuan.getEnlaces(Long.parseLong(id)));
			if (enlaces.getEstado() == 0) {
				mensaje= "El enlace con id:"+id+" ya estba dado de baja";
			}else {
				enlaces.setEstado((byte)0);
				wsPersistenciaJuan.updateEnlaces(enlaces);
				mensaje = "OK";
			}
			
		} catch (Exception e) {
			mensaje = "Ha ocurrido un ERROR inesperado";
		}
		return "{\"mensaje\" : \""+mensaje+"\"}";
	}
	@RequestMapping(value="/reactivacion/{id}" , method=RequestMethod.GET)
	public @ResponseBody String Reactivacion(@PathVariable("id")String id){
		String mensaje;
		try {
			Enlaces enlaces = new Enlaces();
			enlaces = (wsPersistenciaJuan.getEnlaces(Long.parseLong(id)));
			if (enlaces.getEstado() == 1) {
				mensaje= "El enlace con id:"+id+" ya estba activo";
			}else {
				enlaces.setEstado((byte)1);
				wsPersistenciaJuan.updateEnlaces(enlaces);
				mensaje = "OK";
			}
			
		} catch (Exception e) {
			mensaje = "Ha ocurrido un ERROR inesperado";
		}
		
		return "{\"mensaje\" : \""+mensaje+"\"}";
	}
	
}
