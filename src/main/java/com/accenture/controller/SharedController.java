package com.accenture.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.model.Test;
import com.accenture.service.ITestService;
import com.accenture.service.IWSCalculosService;

@Controller
public class SharedController {

	@Autowired
	ITestService testService;
	@Autowired
	IWSCalculosService wsCalculosService;

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String Slash(){
		return "Welcome"
				+ "\n\n"
				+ "/h for help";
	}

	
	
	@RequestMapping(value = "/id={id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Test Index(@PathVariable("id")Long id){
		Test t = new Test();
		try {
			t = testService.getTest(id);
		} catch (Exception e) {
		}
		return t;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Test> Index2(){
		List<Test> t = new ArrayList<Test>();
		try {
			t = testService.listTest();
		} catch (Exception e) {

		}
		return t;
	}

	@RequestMapping(value = "/del={id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String Index3(@PathVariable("id")Long id){

		try {
			testService.deleteTest(id);
			return "Uds borro el test "+id+".";
		} catch (Exception e){
			return "Se produjo un error";
		}

	}

	@RequestMapping(value = "/h", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String help(){
		return "/list\t\t lista todos \n"
				+ "/del={id}\t borra \n"
				+ "/id={id}\t muestra solo uno \n";
	}
	@RequestMapping(value = "/name={name}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String Index(@PathVariable("name")String name){
		
		
		return "Hola, como estas"+name+"?";
	}
}

