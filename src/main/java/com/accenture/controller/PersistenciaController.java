package com.accenture.controller;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.model.Empleado_fe;
import com.accenture.service.WSPersistencia;

@Controller
@RequestMapping("persistencia")
public class PersistenciaController {

	@Autowired
	WSPersistencia wsPersistencia;
	
	/*Alta: el ESTADO no se recibe por parametro se setea por defecto en 1.
	Modificación: solo se puede modificar nombre, apellido, seat, dias_home
	Baja: solo cambia el ESTADO a 0.
	Reactivar: solo cambia el ESTADO a 1.*/
	
	@ExceptionHandler(Exception.class)
	@ResponseBody
	 public String handleIOException(HttpServletResponse httpRes,Throwable ex) {
			httpRes.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return "{}";
	}
	
	@RequestMapping(value = "/alta", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Empleado_fe> Alta(@Valid @RequestBody Empleado_fe empleado, BindingResult errors) {
		if (errors.hasErrors()) {
			return new ResponseEntity<Empleado_fe>(empleado, HttpStatus.BAD_REQUEST);
		}
		wsPersistencia.altaEmpleado(empleado);
		return new ResponseEntity<Empleado_fe>(empleado, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/modificar", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Empleado_fe> Modificar(@Valid @RequestBody Empleado_fe modificado, BindingResult errors) {
		if (errors.hasFieldErrors("idEmpleado")) {
			return new ResponseEntity<Empleado_fe>(modificado, HttpStatus.BAD_REQUEST);
		}
		if (errors.hasFieldErrors("nombre")) {
			modificado.setNombre(null);
		}
		if (errors.hasFieldErrors("apellido")) {
			modificado.setApellido(null);
		}
		if (errors.hasFieldErrors("seat")) {
			modificado.setSeat(null);
		}
		if (errors.hasFieldErrors("dias_home")) {
			modificado.setDias_home(null);
		}
		Empleado_fe empleado = wsPersistencia.modificarEmpleado(modificado.getIdEmpleado(), modificado.getNombre(), modificado.getApellido(), modificado.getSeat(), modificado.getDias_home());
		if (empleado == null) {
			return new ResponseEntity<Empleado_fe>(modificado, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Empleado_fe>(empleado, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/baja", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Empleado_fe> Baja(@Valid @RequestBody Empleado_fe bajado, BindingResult errors) {
		if (errors.hasFieldErrors("idEmpleado")) {
			return new ResponseEntity<Empleado_fe>(bajado, HttpStatus.BAD_REQUEST);
		}
		Empleado_fe empleado = wsPersistencia.bajaEmpleado(bajado.getIdEmpleado());
		if (empleado == null) {
			return new ResponseEntity<Empleado_fe>(bajado, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Empleado_fe>(empleado, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/reactivar", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Empleado_fe> Reactivar(@Valid @RequestBody Empleado_fe reactivado, BindingResult errors) {
		if (errors.hasFieldErrors("idEmpleado")) {
			return new ResponseEntity<Empleado_fe>(reactivado, HttpStatus.BAD_REQUEST);
		}
		Empleado_fe empleado = wsPersistencia.bajaEmpleado(reactivado.getIdEmpleado());
		if (empleado == null) {
			return new ResponseEntity<Empleado_fe>(reactivado, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Empleado_fe>(empleado, HttpStatus.OK);
	}
}

