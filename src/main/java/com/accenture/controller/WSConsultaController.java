package com.accenture.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.accenture.model.Empleado_fe;
import com.accenture.service.IWSConsulta;

@Controller
@RequestMapping("wsconsulta")
public class WSConsultaController {
	@Autowired
	IWSConsulta wsConsulta;

	@RequestMapping(value = "/consulta={dia}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Empleado_fe> consulta1(@PathVariable("dia")String dia){
		
		List <Empleado_fe> listaEmpD = new ArrayList<Empleado_fe>();
	
	
		for (Empleado_fe item : wsConsulta.listaEmpleado()) {
			if(dia.equals(item.getDias_home().toLowerCase())){

				item.getIdEmpleado();
				listaEmpD.add(item);
			} 
		}

		return listaEmpD;
	}

}
