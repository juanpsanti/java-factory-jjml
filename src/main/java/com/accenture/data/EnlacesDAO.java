package com.accenture.data;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.model.Enlaces;

public class EnlacesDAO {
	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public Enlaces getEnlaces(Long id){
		Session s = sessionFactory.openSession();
		Enlaces enlace = s.load(Enlaces.class, id);
		s.close();
		return enlace;
	
	}
	
	public List<Enlaces> listEnlaces(){
		Session s = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Enlaces> listed = s.createQuery("from Enlaces").list();
		s.close();
		return listed;
	}
	
	@Transactional
	public void updateEnlaces(Enlaces enlace){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		s.update(enlace);
		tx.commit();
		s.close();
	}
	
	@Transactional
	public void deleteEnlaces(Long id){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		Enlaces enlace = s.load(Enlaces.class, id);
		s.delete(enlace);
		tx.commit();
		s.close();
	}
	
	@Transactional
	public void insertEnlaces(Enlaces enlace){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		s.save(enlace);
		tx.commit();
		s.close();
	}
}
