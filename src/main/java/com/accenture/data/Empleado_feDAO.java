package com.accenture.data;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.model.Empleado_fe;


public class Empleado_feDAO {
	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public Empleado_fe getEmpleado_fe(Long id){
		Session s = sessionFactory.openSession();
		Empleado_fe empleado = s.load(Empleado_fe.class, id);
		//Hibernate.initialize(empleado.getCaregiverList());
		s.close();
		return empleado;
	
	}
	
	public List<Empleado_fe> listEmpleado_fe(){
		Session s = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Empleado_fe> listed = s.createQuery("from Empleado_fe").list();
		s.close();
		return listed;
	}
	
	@Transactional
	public void updateEmpleado_fe(Empleado_fe empleado){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		s.update(empleado);
		tx.commit();
		s.close();
	}
	
	@Transactional
	public void deleteEmpleado_fe(Long id){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		Empleado_fe empleado = s.load(Empleado_fe.class, id);
		s.delete(empleado);
		tx.commit();
		s.close();
	}
	
	@Transactional
	public void insertEmpleado_fe(Empleado_fe empleado){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		s.save(empleado);
		tx.commit();
		s.close();
	}

}
