package com.accenture.data;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.model.Test;


public class TestDAO {
	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public Test getTest(Long id){
		Session s = sessionFactory.openSession();
		Test test = s.load(Test.class, id);
		//Hibernate.initialize(test.getCaregiverList());
		s.close();
		return test;
	
	}
	
	public List<Test> listTest(){
		Session s = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Test> listed = s.createQuery("from Test").list();
		s.close();
		return listed;
	}
	
	@Transactional
	public void updateTest(Test test){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		s.update(test);
		tx.commit();
		s.close();
	}
	
	@Transactional
	public void deleteTest(Long id){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		Test test = s.load(Test.class, id);
		s.delete(test);
		tx.commit();
		s.close();
	}
	
	@Transactional
	public void insertTest(Test test){
		Session s = sessionFactory.openSession();
		Transaction tx = s.beginTransaction();
		s.save(test);
		tx.commit();
		s.close();
	}

}
